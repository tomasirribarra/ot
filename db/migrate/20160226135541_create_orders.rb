class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.integer :user_id
      t.integer :cliente_id
      t.datetime :delivery
      t.string :medio
      t.string :status
      t.text :obs, :default => ""
      t.timestamps null: false
    end
  end
end
