class CreateClientes < ActiveRecord::Migration
  def change
    create_table :clientes do |t|
      t.string :rut
      t.string :first_name
      t.string :last_name
      t.string :address
      t.string :region
      t.string :city
      t.string :postal_code

      t.timestamps null: false
    end
  end
end
