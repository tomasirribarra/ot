class CreateDetails < ActiveRecord::Migration
  def change
    create_table :details do |t|
      t.integer :order_id
      t.integer :category_id
      t.string :name
      t.string :size, :default => ""
      t.string :weight, :default => ""
      t.string :qty
      t.string :obs, :default => ""
      t.timestamps null: false
    end
  end
end
