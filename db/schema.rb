# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160302050945) do

  create_table "categories", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "clientes", force: :cascade do |t|
    t.string   "rut"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "address"
    t.string   "region"
    t.string   "city"
    t.string   "postal_code"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "details", force: :cascade do |t|
    t.integer  "order_id"
    t.integer  "category_id"
    t.string   "name"
    t.string   "size",        default: ""
    t.string   "weight",      default: ""
    t.string   "qty"
    t.string   "obs",         default: ""
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "events", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "orders", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "cliente_id"
    t.datetime "delivery"
    t.string   "medio"
    t.string   "status"
    t.text     "obs",        default: ""
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

end
