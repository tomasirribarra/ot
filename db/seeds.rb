# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
'''
t.string :rut
t.string :first_name
t.string :last_name
t.string :address
t.string :region
t.string :city
t.string :postal_code
'''


Cliente.create(rut: "16327533-3", first_name: "Alejandro", last_name: "Rodriguez",address: "CARMEN 566", region: "RM", city:"STGO", postal_code: "123123" )
Cliente.create(rut: "17747675-7", first_name: "Sabina", last_name: "Salgado",address: "CARMEN 566", region: "RM", city:"STGO", postal_code: "123123" )
Cliente.create(rut: "11111111-1", first_name: "Segio", last_name: "Rodriguez",address: "CARMEN 566", region: "RM", city:"STGO", postal_code: "123123" )

Event.create(name: "DISEÑO")
Event.create(name: "PINTURA")
Event.create(name: "REMACHADO")
Event.create(name: "LIJADO")

Category.create(name: "SILLAS")
Category.create(name: "MESAS")
Category.create(name: "CAMAS")
Category.create(name: "VELADORES")
Category.create(name: "VELETAS")
