class Detail < ActiveRecord::Base
  belongs_to :order
  has_many :category, dependent: :destroy

  def category
    Category.find(self.category_id)
  end
end
