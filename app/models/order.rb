class Order < ActiveRecord::Base
  belongs_to :cliente
  has_many :details, dependent: :destroy

  def clientes
		Cliente.find(self.cliente_id)
	end
  def details
    Detail.where(order_id: self.id)
  end
end
#respond_with @table.to_json(:methods => [:restaurant,:categories,:products])
