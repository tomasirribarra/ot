class OrdersController < ApplicationController
  def index
  end

  def getallorders
    @orders = Order.all
    render :json => @orders.to_json(:methods => [:clientes,:category])
  end

  def new_orders
    order = params[:order]
    details = params[:details]
    @Order = Order.create(user_id: 123, cliente_id: order["client"]["id"], delivery: order["delivery"], medio: order["medio"], status: "PENDIENTE")
    if @Order
      details.each_with_index do |detail, index|
        @Order.details.create(category_id: detail["category"]["id"], name: detail["name"], size: detail["size"],weight: detail["weight"], qty: detail["qty"], obs: detail["obs"])
      end

      render :json => {:id => @Order["id"] ,:order => @Order, :details=> @Order.details, :result => "ACK"}
    else
      render :json => {:result => "NACK"}
    end
  end
  def show
    @order = Order.find(params[:id])
    render :json =>  @order.to_json(:methods => [:clientes,:details,:category],:include =>{ :details => {:methods => [:category]}})
  end
end
