json.array!(@clientes) do |cliente|
  json.extract! cliente, :id, :rut, :first_name, :last_name, :address, :region, :city, :postal_code, :created_at
  json.url cliente_url(cliente, format: :json)
end
