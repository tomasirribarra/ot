require "rails_helper"

RSpec.feature "Creando Clientes" do
  scenario "un usuario creando un nuevo registro" do
    visit "/"

    click_link "Nuevo Cliente"

    fill_in "rut", with: "16327533-3"
    fill_in "first_name", with: "Alejandro"
    fill_in "last_name", with: "Rodriguez Peña"
    fill_in "address", with: "Carmen 566"
    fill_in "region", with: "RM"
    fill_in "city", with: "Santiago"
    fill_in "postal_code", with: "8725734"

    click_button "Crear Cliente"

    expect(page).to.have_content("El cliente a sido creado")
    expect(page.current_path).to.eq(client_path)

  end
end
