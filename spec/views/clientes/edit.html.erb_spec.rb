require 'rails_helper'

RSpec.describe "clientes/edit", type: :view do
  before(:each) do
    @cliente = assign(:cliente, Cliente.create!(
      :rut => "MyString",
      :first_name => "MyString",
      :last_name => "MyString",
      :address => "MyString",
      :region => "MyString",
      :city => "MyString",
      :postal_code => "MyString"
    ))
  end

  it "renders the edit cliente form" do
    render

    assert_select "form[action=?][method=?]", cliente_path(@cliente), "post" do

      assert_select "input#cliente_rut[name=?]", "cliente[rut]"

      assert_select "input#cliente_first_name[name=?]", "cliente[first_name]"

      assert_select "input#cliente_last_name[name=?]", "cliente[last_name]"

      assert_select "input#cliente_address[name=?]", "cliente[address]"

      assert_select "input#cliente_region[name=?]", "cliente[region]"

      assert_select "input#cliente_city[name=?]", "cliente[city]"

      assert_select "input#cliente_postal_code[name=?]", "cliente[postal_code]"
    end
  end
end
