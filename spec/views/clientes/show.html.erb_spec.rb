require 'rails_helper'

RSpec.describe "clientes/show", type: :view do
  before(:each) do
    @cliente = assign(:cliente, Cliente.create!(
      :rut => "Rut",
      :first_name => "First Name",
      :last_name => "Last Name",
      :address => "Address",
      :region => "Region",
      :city => "City",
      :postal_code => "Postal Code"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Rut/)
    expect(rendered).to match(/First Name/)
    expect(rendered).to match(/Last Name/)
    expect(rendered).to match(/Address/)
    expect(rendered).to match(/Region/)
    expect(rendered).to match(/City/)
    expect(rendered).to match(/Postal Code/)
  end
end
