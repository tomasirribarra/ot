require 'rails_helper'

RSpec.describe "clientes/index", type: :view do
  before(:each) do
    assign(:clientes, [
      Cliente.create!(
        :rut => "Rut",
        :first_name => "First Name",
        :last_name => "Last Name",
        :address => "Address",
        :region => "Region",
        :city => "City",
        :postal_code => "Postal Code"
      ),
      Cliente.create!(
        :rut => "Rut",
        :first_name => "First Name",
        :last_name => "Last Name",
        :address => "Address",
        :region => "Region",
        :city => "City",
        :postal_code => "Postal Code"
      )
    ])
  end

  it "renders a list of clientes" do
    render
    assert_select "tr>td", :text => "Rut".to_s, :count => 2
    assert_select "tr>td", :text => "First Name".to_s, :count => 2
    assert_select "tr>td", :text => "Last Name".to_s, :count => 2
    assert_select "tr>td", :text => "Address".to_s, :count => 2
    assert_select "tr>td", :text => "Region".to_s, :count => 2
    assert_select "tr>td", :text => "City".to_s, :count => 2
    assert_select "tr>td", :text => "Postal Code".to_s, :count => 2
  end
end
