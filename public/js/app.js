var app = angular.module('OtApp', ['ngRoute']);

 // configure our routes
 app.config(function($routeProvider) {
     $routeProvider

         // route for the home page
         .when('/', {
             templateUrl : 'pages/index.html',
             controller  : 'mainController'
         })

         // route for the about page
         .when('/new', {
             templateUrl : 'pages/new_ot.html',
             controller  : 'OTController'
         })
         .when('/print/:id', {
             templateUrl : 'pages/print.html',
             controller  : 'printController'
         })

 });

 // create the controller and inject Angular's $scope
 app.controller('mainController', function($scope, $http) {
     // create a message to display in our view
     $scope.statuses = ["PENDIENTE", "PRODUCCION", "FINALIZADO", "DESPACHADO"];
     $scope.medios = ["INTERNET", "TIENDA", "FACEBOOK", "REFERIDOS"];

    $http.get('/orders/all').success(function(data){
      $scope.orders = data;
    });
 });

 app.controller('OTController', function($scope, $http) {
     $scope.categories = [];
     $scope.clients = [];
     $scope.client = {};
     $scope.products = [];
     $scope.product = {};
     $scope.medios = ["INTERNET", "TIENDA", "FACEBOOK", "REFERIDOS"];

     $http.get('/clientes.json').success(function(d){
       $scope.clients = d;
     });
     $http.get('/categories.json').success(function(d){
       $scope.categories = d;
       console.log(d);
     });

     $scope.client_enter = function(){
       $http.get('/clientes.json').success(function(d){
         $scope.clients = d;
         var find = false;
         angular.forEach($scope.clients, function(o,i){
            if(o.rut == $scope.client.rut){
              $scope.client = o;
              find = true;
            }
         });
         if (!find) $scope.client = {};
       });


     };

     $scope.saveProduct = function(){
       $scope.error_product = false;
       var a = {};
       a = $scope.product;

       if(!a.category) {
          $scope.error_product = true;
         return;
       }
       if(!a.name) {
          $scope.error_product = true;
         return;
       }
       if(!a.qty) {
        $scope.error_product = true;
        return;
       }
       if (a.qty == 0) {
         $scope.error_product = true;
         return;
       }


       $scope.product = {};
       $scope.products.push(a);
     }

     $scope.selectClient = function(p){
       $scope.client = p;
     }
     $scope.editProduct = function(p){
          $scope.client = p;
     }
     $scope.deleteProduct = function(p){
       for (var i =0; i < $scope.products.length; i++) {
           if ($scope.products[i] === p) {
              $scope.products.splice(i,1);
              break;
           }
       }
     }
     $scope.saveOT = function() {
       $scope.error_ot = false;

         if(!$scope.client.rut) {
           $scope.msg = "Seleccione el Cliente";
           $scope.error_ot = true;
           return;
         }

        if(!$scope.medio) {
          $scope.msg = "Seleccione el medio";
          $scope.error_ot = true;
          return;
        }
        if(!$scope.delivery) {
          $scope.msg = "Seleccione el Fecha de Entrega Aproximada";
          $scope.error_ot = true;
          return;
        }


        if($scope.products.length == 0) {
          $scope.msg = "Agrege un producto al detalle";
          $scope.error_ot = true;
          return;
        }

        var ot = {};

        ot.order = { client : $scope.client , medio: $scope.medio , delivery:  $scope.delivery , obs: $scope.obs};
        ot.details = $scope.products;

        $http.post("/orders/new/" , ot).success(function(p){
          console.log(p);
          if(p.result = "ACK") {
            location.href ="/orders#/print/" + p.id;
          }

        });


     };
 });

 app.controller('printController', function($scope,$http,$routeParams) {
   $scope.id = $routeParams.id;
   $http.get('/orders/show/' + $scope.id).success(function(data){
     $scope.order = data;
   });
 });




app.directive('ngEnter', function() {
         return function(scope, element, attrs) {
             element.bind("keydown keypress", function(event) {
                 if(event.which === 13) {
                     scope.$apply(function(){
                         scope.$eval(attrs.ngEnter, {'event': event});
                     });

                     event.preventDefault();
                 }
             });
         };
     });
